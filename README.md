# Utils

Utils is a small golang library that was designed to be useful for myself. It will grow organically as I need it to.

So far, functionality includes

- Basic arrays (1D, 2D, and 3D). Arrays are dense and stored in a single Go slice in a contiguous block of memory.  Get and Set methods provide array access and allow for the future possibility of choosing C or FORTRAN row/column ordering. This will be addressed later when access to those languages is needed.
- IO utilities. So far, a thin wrapper around Go's standard binary read and write functions.
- A few helpful geometry structures and methods.