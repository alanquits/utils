package geometry

import (
	"math"
	"testing"
)

func TestPoint2DAdd(t *testing.T) {
	p1 := NewPoint2D(2.0, 5.0)
	p2 := NewPoint2D(-1.0, 2.7)
	want := NewPoint2D(1.0, 7.7)
	got := p1.Add(p2)
	if !want.Equals(got, 1e-12) {
		t.Fatal("TestPoint2DAdd failed")
	}
}

func TestPoint2DDotProduct(t *testing.T) {
	p1 := NewPoint2D(1.0, 8.0)
	p2 := NewPoint2D(4.8, -1.0)
	want := 1.0*4.8 + 8.0*(-1.0)
	got := p1.DotProduct(p2)
	if math.Abs(want-got) > 1e-12 {
		t.Fatal("TestPoint2DDotProduct failed")
	}
}

func TestPoint2DL2Norm(t *testing.T) {
	p := NewPoint2D(3.0, 4.0)
	want := 5.0
	got := p.L2Norm()
	if math.Abs(want-got) > 1e-12 {
		t.Fatal("TestPoint2DL2Norm failed")
	}
}

func TestPoint2DManhattanNorm(t *testing.T) {
	p := NewPoint2D(-1.5, -3.0)
	want := 4.5
	got := p.ManhattanNorm()
	if math.Abs(want-got) > 1e-12 {
		t.Fatal("TestPoint2DManhattanNorm failed")
	}
}

func TestPoint2DMin(t *testing.T) {
	p1 := NewPoint2D(3.0, -1.0)
	p2 := NewPoint2D(2.0, 8.8)
	want := NewPoint2D(2.0, -1.0)
	got := p1.Min(p2)
	if !want.Equals(got, 1e-12) {
		t.Fatal("TestPoint2DMax failed")
	}
}

func TestPoint2DMax(t *testing.T) {
	p1 := NewPoint2D(3.0, -1.0)
	p2 := NewPoint2D(2.0, 8.8)
	want := NewPoint2D(3.0, 8.8)
	got := p1.Max(p2)
	if !want.Equals(got, 1e-12) {
		t.Fatal("TestPoint2DMax failed")
	}
}

func TestPoint2DSub(t *testing.T) {
	p1 := NewPoint2D(2.0, 3.0)
	p2 := NewPoint2D(1.0, 5.0)
	want := NewPoint2D(1.0, -2.0)
	got := p1.Sub(p2)
	if !want.Equals(got, 1e-12) {
		t.Fatal("TestPoint2DSub failed")
	}
}

func TestPoint2DRotate(t *testing.T) {
	p := NewPoint2D(5.0, 0.0)
	want := NewPoint2D(0.0, 5.0)
	got := p.Rotate2D(NewPoint2D(0.0, 0.0), math.Pi/2.0)
	if !want.Equals(got, 1e-12) {
		t.Fatal("TestPoint2DSub failed")
	}
}
