package geometry

import (
	"errors"
	"fmt"
	"strings"
)

type Polygon struct {
	Points []Point2D
}

func NewPolygon(points []Point2D) (*Polygon, error) {
	p := &Polygon{}

	if len(points) < 4 {
		return p, errors.New("NewPolygon must be called with at least four points")
	}

	if !points[0].Equals(points[len(points)-1], 1e-12) {
		return p, errors.New("first and last point in polygon must be the same")
	}

	// TODO: check for polygons with overlapping edges?
	p.Points = points
	return p, nil
}

func (p *Polygon) BoundingBox2D() *Box2D {
	return NewBox2D(p.Min(), p.Max())
}

func (p *Polygon) Max() Point2D {
	pMax := p.Points[0]
	for _, p := range p.Points {
		pMax = pMax.Max(p)
	}
	return pMax
}

func (p *Polygon) Min() Point2D {
	pMin := p.Points[0]
	for _, p := range p.Points {
		pMin = pMin.Min(p)
	}
	return pMin
}

func (p *Polygon) Wkt() string {
	var sb strings.Builder
	sb.WriteString("POLYGON ((")
	for i, pt := range p.Points {
		if i > 0 {
			sb.WriteString(", ")
		}
		sb.WriteString(fmt.Sprintf("%f %f", pt.X, pt.Y))
	}
	sb.WriteString("))")
	return sb.String()
}
