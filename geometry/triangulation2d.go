package geometry

import "errors"

type Triangulation2D struct {
	points    []Point2D
	triangles [][3]int
}

func NewTriangulation2D(points []Point2D, triangles [][3]int) (*Triangulation2D, error) {
	// TODO: check errors
	return &Triangulation2D{
		points:    points,
		triangles: triangles,
	}, nil
}

func (tg *Triangulation2D) Points() []Point2D {
	return tg.points
}

func (tg *Triangulation2D) Triangles() [][3]int {
	return tg.triangles
}

func (tg *Triangulation2D) Triangle(idx int) (*Triangle, error) {
	if idx < 0 || idx >= len(tg.triangles) {
		return &Triangle{}, errors.New("index is out of range")
	}

	p1Idx := tg.triangles[idx][0]
	p2Idx := tg.triangles[idx][1]
	p3Idx := tg.triangles[idx][2]

	p1, p2, p3 := tg.points[p1Idx], tg.points[p2Idx], tg.points[p3Idx]

	return NewTriangle(p1, p2, p3), nil
}
