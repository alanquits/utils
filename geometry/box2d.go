package geometry

import "fmt"

type Box2D struct {
	lower Point2D
	upper Point2D
}

func NewBox2D(p1 Point2D, p2 Point2D) *Box2D {
	return &Box2D{
		lower: p1.Min(p2),
		upper: p1.Max(p2),
	}
}

func (b *Box2D) Lower() Point2D {
	return b.lower
}

func (b *Box2D) Upper() Point2D {
	return b.upper
}

func (b *Box2D) Area() float64 {
	dx := b.upper.X - b.lower.X
	dy := b.upper.Y - b.lower.Y
	return dx * dy
}

func (b *Box2D) Wkt() string {
	p0 := b.Lower()
	pf := b.Upper()
	x0, y0 := p0.X, p0.Y
	xf, yf := pf.X, pf.Y
	return fmt.Sprintf("POLYGON ((%f %f, %f %f, %f %f, %f %f, %f, %f))",
		x0, y0, xf, y0, xf, yf, x0, yf, x0, y0)
}
