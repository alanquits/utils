package geometry

import (
	"fmt"
	"math"
)

type Point3D struct {
	X float64
	Y float64
	Z float64
}

func (p Point3D) Add(other Point3D) Point3D {
	return Point3D{
		X: p.X + other.X,
		Y: p.Y + other.Y,
		Z: p.Z + other.Z,
	}
}

func (p Point3D) DotProduct(other Point3D) float64 {
	return p.X*other.X + p.Y*other.Y + p.Z*other.Z
}

func (p Point3D) Max(other Point3D) Point3D {
	return Point3D{
		X: math.Max(p.X, other.X),
		Y: math.Max(p.Y, other.Y),
		Z: math.Max(p.Z, other.Z),
	}
}

func (p Point3D) Min(other Point3D) Point3D {
	return Point3D{
		X: math.Min(p.X, other.X),
		Y: math.Min(p.Y, other.Y),
		Z: math.Min(p.Z, other.Z),
	}
}

func (p Point3D) Area() float64 {
	return 0.0
}

func (p Point3D) Wkt() string {
	return fmt.Sprintf("POINT Z (%f %f %f)", p.X, p.Y, p.Z)
}
