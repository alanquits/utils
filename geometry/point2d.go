package geometry

import (
	"fmt"
	"math"
)

type Point2D struct {
	X float64
	Y float64
}

func NewPoint2D(x float64, y float64) Point2D {
	return Point2D{
		X: x,
		Y: y,
	}
}

func (p Point2D) Add(other Point2D) Point2D {
	return Point2D{
		X: p.X + other.X,
		Y: p.Y + other.Y,
	}
}

func (p Point2D) DotProduct(other Point2D) float64 {
	return p.X*other.X + p.Y*other.Y
}

func (p Point2D) Equals(other Point2D, tolerance float64) bool {
	return p.Sub(other).ManhattanNorm() < tolerance
}

func (p Point2D) L2Norm() float64 {
	return math.Sqrt(p.X*p.X + p.Y*p.Y)
}

func (p Point2D) ManhattanNorm() float64 {
	return math.Abs(p.X) + math.Abs(p.Y)
}

func (p Point2D) Max(other Point2D) Point2D {
	return Point2D{
		X: math.Max(p.X, other.X),
		Y: math.Max(p.Y, other.Y),
	}
}

func (p Point2D) Min(other Point2D) Point2D {
	return Point2D{
		X: math.Min(p.X, other.X),
		Y: math.Min(p.Y, other.Y),
	}
}

func (p Point2D) Sub(other Point2D) Point2D {
	return Point2D{
		X: p.X - other.X,
		Y: p.Y - other.Y,
	}
}

func (p Point2D) Area() float64 {
	return 0.0
}

func (p Point2D) Rotate2D(pointOfRotation Point2D, angle float64) Point2D {
	p = p.Sub(pointOfRotation)
	c := math.Cos(angle)
	s := math.Sin(angle)
	xNew := p.X*c - p.Y*s
	yNew := p.Y*c + p.X*s
	return Point2D{
		X: xNew,
		Y: yNew,
	}.Add(pointOfRotation)
}

func (p Point2D) Wkt() string {
	return fmt.Sprintf("POINT (%f %f)", p.X, p.Y)
}
