package geometry

type Triangle struct {
	P1 Point2D
	P2 Point2D
	P3 Point2D
}

func NewTriangle(p1, p2, p3 Point2D) *Triangle {
	return &Triangle{
		P1: p1,
		P2: p2,
		P3: p3,
	}
}

func (t *Triangle) Centroid() Point2D {
	x := (t.P1.X + t.P2.X + t.P3.X) / 3.0
	y := (t.P1.Y + t.P2.Y + t.P3.Y) / 3.0
	return NewPoint2D(x, y)
}

