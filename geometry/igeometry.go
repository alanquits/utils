package geometry

type Geometry interface {
	Area() float64
	BoundingBox2D() Box2D
	// BoundingBox3D() Box3D
	Wkt() string
}
