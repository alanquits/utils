package binary

import (
	"encoding/binary"
	"log"
	"os"
)

type BinaryReader struct {
	Filename string
	File     *os.File
	Order    binary.ByteOrder
	Err      error
}

func NewReader(filename string, order binary.ByteOrder) (*BinaryReader, error) {
	file, err := os.Open(filename)
	if err != nil {
		return &BinaryReader{}, err
	}

	return &BinaryReader{
		Filename: filename,
		File:     file,
		Order:    order,
		Err:      nil,
	}, nil
}

func NewReader_(filename string, order binary.ByteOrder) *BinaryReader {
	br, err := NewReader(filename, order)
	if err != nil {
		log.Fatal(err)
	}
	return br
}

func (br *BinaryReader) Close() {
	br.File.Close()
}

func (br *BinaryReader) Read(nums ...any) {
	if br.Err != nil {
		return
	}
	for _, num := range nums {
		err := binary.Read(br.File, br.Order, num)
		if err != nil {
			br.Err = err
		}
	}
}

func (br *BinaryReader) Error() error {
	return br.Err
}
