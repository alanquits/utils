package binary

import (
	"encoding/binary"
	"log"
	"os"
)

type BinaryWriter struct {
	Filename string
	File     *os.File
	Order    binary.ByteOrder
	Err      error
}

func NewWriter(filename string, order binary.ByteOrder) (*BinaryWriter, error) {
	file, err := os.Create(filename)
	if err != nil {
		return &BinaryWriter{}, err
	}

	return &BinaryWriter{
		Filename: filename,
		File:     file,
		Order:    order,
		Err:      nil,
	}, nil
}

func NewWriter_(filename string, order binary.ByteOrder) *BinaryWriter {
	bw, err := NewWriter(filename, order)
	if err != nil {
		log.Fatal(err)
	}
	return bw
}

func (bw *BinaryWriter) Close() {
	bw.File.Close()
}

func (bw *BinaryWriter) Write(nums ...any) {
	if bw.Err != nil {
		return
	}
	for _, num := range nums {
		err := binary.Write(bw.File, bw.Order, num)
		if err != nil {
			bw.Err = err
		}
	}
}

func (bw *BinaryWriter) Error() error {
	return bw.Err
}
