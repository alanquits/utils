package arrays

import (
	"fmt"
	"strings"
)

type Array3D[T any] struct {
	nx   int
	ny   int
	nz   int
	data []T
}

func NewArray3D[T any](nx int, ny int, nz int, initialValue T) *Array3D[T] {
	if nx <= 0 || ny <= 0 || nz <= 0 {
		panic("Tried to initialize Array3D with 0 or negative size")
	}

	arr := &Array3D[T]{}
	arr.data = make([]T, nx*ny*nz)
	arr.nx = nx
	arr.ny = ny
	arr.nz = nz
	for i := 0; i < nx*ny*nz; i++ {
		arr.data[i] = initialValue
	}

	return arr
}

func (arrIn *Array3D[T]) Copy() *Array3D[T] {
	arrOut := &Array3D[T]{}
	arrOut.nx = arrIn.nx
	arrOut.ny = arrIn.ny
	arrOut.nz = arrIn.nz

	arrOut.data = make([]T, arrOut.nx*arrOut.ny*arrOut.nz)
	for i := 0; i < arrOut.nx*arrOut.ny*arrOut.nz; i++ {
		arrOut.data[i] = arrIn.data[i]
	}

	return arrOut
}

func (arr *Array3D[T]) flattenIndex(ix, iy, iz int) int {
	return (iz * arr.nx * arr.ny) + (iy * arr.nx) + ix
}

func (arr *Array3D[T]) Get(ix, iy, iz int) T {
	arr.validateIndex(ix, iy, iz)
	i := arr.flattenIndex(ix, iy, iz)
	return arr.data[i]
}

func (arr *Array3D[T]) Iter(f func(T)) {
	for ix := 0; ix < arr.nx; ix++ {
		for iy := 0; iy < arr.ny; iy++ {
			for iz := 0; iz < arr.nz; iz++ {
				i := arr.flattenIndex(ix, iy, iz)
				f(arr.data[i])
			}
		}
	}
}

func (arr *Array3D[T]) IterI(f func(int, int, int, T)) {
	for ix := 0; ix < arr.nx; ix++ {
		for iy := 0; iy < arr.ny; iy++ {
			for iz := 0; iz < arr.nz; iz++ {
				i := arr.flattenIndex(ix, iy, iz)
				f(ix, iy, iz, arr.data[i])
			}
		}
	}
}

func (arrIn *Array3D[T]) Map(f func(T) T) *Array3D[T] {
	arrOut := &Array3D[T]{}
	arrOut.nx = arrIn.nx
	arrOut.ny = arrIn.ny
	arrOut.nz = arrIn.nz

	arrOut.data = make([]T, arrOut.nx*arrOut.ny*arrOut.nz)
	for i := 0; i < arrOut.nx*arrOut.ny*arrOut.nz; i++ {
		arrOut.data[i] = f(arrIn.data[i])
	}

	return arrOut
}

func (arrIn *Array3D[T]) MapI(f func(int, int, int, T) T) *Array3D[T] {
	arrOut := &Array3D[T]{}
	arrOut.nx = arrIn.nx
	arrOut.ny = arrIn.ny
	arrOut.nz = arrIn.nz

	arrOut.data = make([]T, arrOut.nx*arrOut.ny*arrOut.nz)

	for ix := 0; ix < arrOut.nx; ix++ {
		for iy := 0; iy < arrOut.ny; iy++ {
			for iz := 0; iz < arrOut.nz; iz++ {
				i := arrOut.flattenIndex(ix, iy, iz)
				arrOut.data[i] = f(ix, iy, iz, arrIn.data[i])
			}
		}
	}

	return arrOut
}

func (arr *Array3D[T]) Nx() int {
	return arr.nx
}

func (arr *Array3D[T]) Ny() int {
	return arr.ny
}

func (arr *Array3D[T]) Nz() int {
	return arr.nz
}

func (arr *Array3D[T]) Set(ix, iy, iz int, v T) {
	arr.validateIndex(ix, iy, iz)
	i := arr.flattenIndex(ix, iy, iz)
	arr.data[i] = v
}

func (arr *Array3D[T]) String() string {
	var sb strings.Builder
	sb.WriteByte('[')
	for ix := 0; ix < arr.nx; ix++ {
		if ix > 0 {
			sb.WriteString(", ")
		}
		sb.WriteByte('[')
		for iy := 0; iy < arr.ny; iy++ {
			if iy > 0 {
				sb.WriteString(", ")
			}
			sb.WriteByte('[')
			for iz := 0; iz < arr.nz; iz++ {
				if iz > 0 {
					sb.WriteString(", ")
				}
				i := arr.flattenIndex(ix, iy, iz)
				sb.WriteString(fmt.Sprintf("%v", arr.data[i]))
			}
			sb.WriteByte(']')

		}
		sb.WriteByte(']')
	}
	sb.WriteByte(']')
	return sb.String()
}

func (arr *Array3D[T]) validateIndex(ix, iy, iz int) {
	xOk := 0 <= ix && ix < arr.nx
	yOk := 0 <= iy && iy < arr.ny
	zOk := 0 <= iz && iz < arr.nz
	if !(xOk && yOk && zOk) {
		panic("attempted to access an out of bounds index in Array3D")
	}
}
