package arrays

import (
	"fmt"
	"strings"
)

type Array2D[T any] struct {
	nx   int
	ny   int
	data []T
}

func NewArray2D[T any](nx int, ny int, initialValue T) *Array2D[T] {
	if nx <= 0 || ny <= 0 {
		panic("Tried to initialize Array2D with 0 or negative size")
	}

	arr := &Array2D[T]{}
	arr.data = make([]T, nx*ny)
	arr.nx = nx
	arr.ny = ny

	for i := 0; i < nx*ny; i++ {
		arr.data[i] = initialValue
	}

	return arr
}

func (arrIn *Array2D[T]) Copy() *Array2D[T] {
	arrOut := &Array2D[T]{}
	arrOut.data = make([]T, arrIn.nx*arrIn.ny)
	arrOut.nx = arrIn.nx
	arrOut.ny = arrIn.ny

	for i := 0; i < arrOut.nx*arrOut.ny; i++ {
		arrOut.data[i] = arrIn.data[i]
	}

	return arrOut
}

func (arr *Array2D[T]) flattenIndex(ix, iy int) int {
	return arr.nx*iy + ix
}

func (arr *Array2D[T]) Get(ix, iy int) T {
	arr.validateIndex(ix, iy)
	i := arr.flattenIndex(ix, iy)
	return arr.data[i]
}

func (arr *Array2D[T]) Iter(f func(T)) {
	// It would have been possible to loop through this array using one index, but this
	// method makes the looping order predictable in the potential future case that Get and Set
	// are modified to allow C and Fortran memory layout.
	for ix := 0; ix < arr.nx; ix++ {
		for iy := 0; iy < arr.ny; iy++ {
			i := arr.flattenIndex(ix, iy)
			f(arr.data[i])
		}
	}
}

func (arr *Array2D[T]) IterI(f func(int, int, T)) {
	for ix := 0; ix < arr.nx; ix++ {
		for iy := 0; iy < arr.ny; iy++ {
			i := arr.flattenIndex(ix, iy)
			f(ix, iy, arr.data[i])
		}
	}
}

func (arrIn *Array2D[T]) Map(f func(T) T) *Array2D[T] {
	arrOut := &Array2D[T]{}
	arrOut.nx = arrIn.nx
	arrOut.ny = arrIn.ny
	arrOut.data = make([]T, arrIn.nx*arrIn.ny)

	for ix := 0; ix < arrIn.nx; ix++ {
		for iy := 0; iy < arrIn.ny; iy++ {
			i := arrIn.flattenIndex(ix, iy)
			arrOut.data[i] = f(arrIn.data[i])
		}
	}

	return arrOut
}

func (arrIn *Array2D[T]) MapI(f func(int, int, T) T) *Array2D[T] {
	arrOut := &Array2D[T]{}
	arrOut.nx = arrIn.nx
	arrOut.ny = arrIn.ny
	arrOut.data = make([]T, arrIn.nx*arrIn.ny)

	for ix := 0; ix < arrIn.nx; ix++ {
		for iy := 0; iy < arrIn.ny; iy++ {
			i := arrIn.flattenIndex(ix, iy)
			arrOut.data[i] = f(ix, iy, arrIn.data[i])
		}
	}

	return arrOut
}

func (arr *Array2D[T]) Nx() int {
	return arr.nx
}

func (arr *Array2D[T]) Ny() int {
	return arr.ny
}

func (arr *Array2D[T]) Set(ix, iy int, v T) {
	arr.validateIndex(ix, iy)
	i := arr.flattenIndex(ix, iy)
	arr.data[i] = v
}

func (arr *Array2D[T]) String() string {
	var sb strings.Builder
	sb.WriteByte('[')
	for ix := 0; ix < arr.nx; ix++ {
		if ix > 0 {
			sb.WriteString(", ")
		}
		sb.WriteByte('[')
		for iy := 0; iy < arr.ny; iy++ {
			if iy > 0 {
				sb.WriteString(", ")
			}
			i := arr.flattenIndex(ix, iy)
			sb.WriteString(fmt.Sprintf("%v", arr.data[i]))
		}
		sb.WriteByte(']')
	}
	sb.WriteByte(']')
	return sb.String()
}

func (arr *Array2D[T]) validateIndex(ix, iy int) {
	xOk := 0 <= ix && ix < arr.nx
	yOk := 0 <= iy && iy < arr.ny
	if !(xOk && yOk) {
		panic("attempted to access an out of bounds index in Array2D")
	}
}
