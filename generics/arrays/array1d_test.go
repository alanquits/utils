package arrays

import (
	"fmt"
	"math"
	"strings"
	"testing"
)

func TestArray1D_string(t *testing.T) {
	arr := NewArray1D[int32](10, 5)
	got := arr.String()
	want := "[5, 5, 5, 5, 5, 5, 5, 5, 5, 5]"
	if got != want {
		t.Errorf("want %q, got %q", want, got)
	}
}

func TestArray1D_copy(t *testing.T) {
	arrOriginal := NewArray1D[int32](3, 0)
	arrCopy := arrOriginal.Copy()
	arrCopy.Set(1, 5)

	if !(arrOriginal.Get(1) == 0 && arrCopy.Get(1) == 5) {
		t.Error("copy test failed")
	}
}

func TestArray1D_iter(t *testing.T) {
	arr := NewArray1D[int32](3, 0)
	var sb strings.Builder
	arr.Set(0, 1)
	arr.Set(1, 2)
	arr.Set(2, 3)
	arr.Iter(func(v int32) {
		sb.WriteString(fmt.Sprintf("%d ", v*v))
	})
	got := sb.String()
	want := "1 4 9 "
	if got != want {
		t.Errorf("want %q, got %q", want, got)
	}
}

func TestArray1D_iteri(t *testing.T) {
	arr := NewArray1D[int32](3, 0)
	arr.IterI(func(i int, v int32) {
		arr.Set(i, int32(2*i))
	})
	got := arr.String()
	want := "[0, 2, 4]"
	if got != want {
		t.Errorf("want %q, got %q", want, got)
	}
}

func TestArray1D_map(t *testing.T) {
	arr := NewArray1D[int32](10, 0)
	arr.IterI(func(i int, v int32) {
		arr.Set(i, int32(i))
	})
	arr = arr.Map(func(v int32) int32 { return v * v })
	got := arr.String()
	want := "[0, 1, 4, 9, 16, 25, 36, 49, 64, 81]"
	if got != want {
		t.Errorf("want %q, got %q", want, got)
	}
}

func TestArray1D_mapi(t *testing.T) {
	arr := NewArray1D[int32](5, 0)
	arr = arr.MapI(func(i int, v int32) int32 {
		return int32(3 * i)
	})
	got := arr.String()
	want := "[0, 3, 6, 9, 12]"
	if got != want {
		t.Errorf("want %q, got %q", want, got)
	}
}

func TestArray1D_linspace(t *testing.T) {
	arr1 := Linspace(0, 3, 7)
	arr2 := NewArray1D(7, 0.0)
	arr2.Set(0, 0.0)
	arr2.Set(1, 0.5)
	arr2.Set(2, 1.0)
	arr2.Set(3, 1.5)
	arr2.Set(4, 2.0)
	arr2.Set(5, 2.5)
	arr2.Set(6, 3.0)
	totalError := 0.0
	arr1.IterI(func(i int, v float64) {
		totalError += math.Abs(arr1.Get(i) - arr2.Get(i))
	})
	if totalError > 1e-20 {
		t.Errorf("wanted error less than 1e-20 in linspace, got error of %f", totalError)
	}
}
