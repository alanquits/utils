package arrays

import (
	"fmt"
	"strings"
)

type Array1D[T any] struct {
	length int
	data   []T
}

func NewArray1D[T any](length int, initialValue T) *Array1D[T] {
	if length <= 0 {
		panic("Tried to initialize Array1D with 0 or negative size")
	}

	arr := &Array1D[T]{}
	arr.data = make([]T, length)
	arr.length = length
	for i := range arr.data {
		arr.data[i] = initialValue
	}

	return arr
}

func (arr *Array1D[T]) Get(i int) T {
	if !(0 <= i && i < arr.length) {
		panic("attempted to access an invalid index in Array1D")
	}
	return arr.data[i]
}

func (arrIn *Array1D[T]) Copy() *Array1D[T] {
	arrOut := &Array1D[T]{}
	arrOut.data = make([]T, arrIn.length)
	arrOut.length = arrIn.length

	for i := range arrOut.data {
		arrOut.data[i] = arrIn.data[i]
	}

	return arrOut
}

func (arr *Array1D[T]) Iter(f func(T)) {
	for i := 0; i < arr.length; i++ {
		f(arr.data[i])
	}
}

func (arr *Array1D[T]) IterI(f func(int, T)) {
	for i := 0; i < arr.length; i++ {
		f(i, arr.data[i])
	}
}

func (arr *Array1D[T]) Length() int {
	return arr.length
}

func (arrIn *Array1D[T]) Map(f func(T) T) *Array1D[T] {
	arrOut := &Array1D[T]{}
	arrOut.length = arrIn.length
	arrOut.data = make([]T, arrOut.length)
	for i := range arrOut.data {
		arrOut.data[i] = f(arrIn.data[i])
	}
	return arrOut
}

func (arrIn *Array1D[T]) MapI(f func(int, T) T) *Array1D[T] {
	arrOut := &Array1D[T]{}
	arrOut.length = arrIn.length
	arrOut.data = make([]T, arrOut.length)
	for i := range arrOut.data {
		arrOut.data[i] = f(i, arrIn.data[i])
	}
	return arrOut
}

func (arr *Array1D[T]) Set(i int, v T) {
	if !(0 <= i && i < arr.length) {
		panic("attempted to access an invalid index in Array1D")
	}
	arr.data[i] = v

}

func (arr *Array1D[T]) String() string {
	var sb strings.Builder
	sb.WriteByte('[')
	for i := range arr.data {
		if i > 0 {
			sb.WriteString(", ")
		}
		sb.WriteString(fmt.Sprintf("%v", arr.data[i]))
	}
	sb.WriteByte(']')
	return sb.String()
}

func Linspace(v0 float64, vf float64, n int) *Array1D[float64] {
	arr := NewArray1D(n, 0.0)
	dx := (vf - v0) / float64(n-1)
	return arr.MapI(func(i int, v float64) float64 {
		return v0 + float64(i)*dx
	})
}
