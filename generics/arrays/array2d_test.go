package arrays

import (
	"fmt"
	"strings"
	"testing"
)

func TestArray2D_string(t *testing.T) {
	arr := NewArray2D[int32](2, 3, 12)
	got := arr.String()
	want := "[[12, 12, 12], [12, 12, 12]]"
	if got != want {
		t.Errorf("want %q, got %q", want, got)
	}
}

func TestArray2D_copy(t *testing.T) {
	arrOriginal := NewArray2D[int32](2, 3, 0)
	arrCopy := arrOriginal.Copy()
	arrCopy.Set(1, 1, 5)

	if !(arrOriginal.Get(1, 1) == 0 && arrCopy.Get(1, 1) == 5) {
		t.Error("copy test failed")
	}
}

func TestArray2D_iter(t *testing.T) {
	arr := NewArray2D[int32](1, 3, 0)
	var sb strings.Builder
	arr.Set(0, 0, 1)
	arr.Set(0, 1, 2)
	arr.Set(0, 2, 3)
	arr.Iter(func(v int32) {
		sb.WriteString(fmt.Sprintf("%d ", v*v))
	})
	got := sb.String()
	want := "1 4 9 "
	if got != want {
		t.Errorf("want %q, got %q", want, got)
	}
}

func TestArray2D_iteri(t *testing.T) {
	arr := NewArray2D[int32](3, 2, 0)
	arr.IterI(func(i int, j int, v int32) {
		arr.Set(i, j, int32(2*i+j))
	})
	got := arr.String()
	want := "[[0, 1], [2, 3], [4, 5]]"
	if got != want {
		t.Errorf("want %q, got %q", want, got)
	}
}

func TestArray2D_map(t *testing.T) {
	arr := NewArray2D[int32](1, 10, 0)
	arr.IterI(func(i int, j int, v int32) {
		arr.Set(i, j, int32(j))
	})
	arr = arr.Map(func(v int32) int32 { return v * v })
	got := arr.String()
	want := "[[0, 1, 4, 9, 16, 25, 36, 49, 64, 81]]"
	if got != want {
		t.Errorf("want %q, got %q", want, got)
	}
}

func TestArray2D_mapi(t *testing.T) {
	arr := NewArray2D[int32](2, 2, 0)
	arr = arr.MapI(func(i int, j int, v int32) int32 {
		return int32(3*i + 2*j)
	})
	got := arr.String()
	want := "[[0, 2], [3, 5]]"
	if got != want {
		t.Errorf("want %q, got %q", want, got)
	}
}
